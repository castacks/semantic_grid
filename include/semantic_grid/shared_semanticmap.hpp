#ifndef SHARED_SEMANTICMAP_HPP_9FWOQEMW
#define SHARED_SEMANTICMAP_HPP_9FWOQEMW

#include <cmath>

#include <ros/ros.h>
#include <ros/console.h>

#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/sync/named_semaphore.hpp>

#include <scrollgrid/fixedgrid2.hpp>
#include <scrollgrid/dense_array2.hpp>

#include <semantic_grid/semantic_map_datatypes.h>
#include <scrollgrid/raycasting.hpp>

#include <local_frame_manager/local_frame_manager.h>
#include <dem_reader/dem.h>
namespace ca
{

// TODO make this configurable

class SharedSemanticMap {
public:
    double min_update_distance_;
    std::vector<size_t> mem_to_process_;
    void AddToBeProcessed(size_t mem);
    void ProcessToBeProcessed();
  SharedSemanticMap() : parameters_() { }
  SharedSemanticMap(std::string name, ca::Vec2Ix dim, float res):
    parameters_(name,dim,res) { }

  double GetRayHeight(Eigen::Vector3d& sensor_pos, Eigen::Vector3d& dir, Eigen::Vector2d& xy, bool &vertical);
  virtual void ProcessRay(Eigen::Vector3d &dir, Eigen::Vector3d &pos, double range, SemanticClass &c, double prob);
  virtual bool UpdateCell(Eigen::Vector3d& sensor_pos, Eigen::Vector3d& dir, ca::SemanticClass &c, int x, int y, bool endpt);
  virtual bool UpdateCell3D(ca::SemanticClass &c, double p,int x, int y, int z, int s_x, int s_y, int s_z);

  bool init(float x_size, float y_size, float resolution, Eigen::Vector2f origin, LocalFrameManager *lfm = NULL, DEMData *dem = NULL) {

    namespace bipc = boost::interprocess;
    parameters_.origin = origin;
    parameters_.resolution = resolution;
    parameters_.dimension.x() = x_size;
    parameters_.dimension.y() = y_size;
    init_grid_semaphore_name_=parameters_.shm_name +"_init_semantic_semaphore";
    init_semaphore_= new bipc::named_semaphore(bipc::open_or_create,init_grid_semaphore_name_.c_str(),0);
    size_t to_allocate = 0;
    to_allocate += sizeof(FixedGrid2f);
    to_allocate += sizeof(DenseArray2<SemanticGridCell>);
    to_allocate += sizeof(SemanticGridCell)*(parameters_.dimension.prod());
    to_allocate += 65535; // arbitrary buffer to account for my stupidity
    // nearest power of 2 -- desirable? necessary?
    //size_t to_allocate = (pow(2, ceil(log2(to_allocate))));
    to_allocate += (4096 - (to_allocate % 4096));
    ROS_INFO_STREAM("to_allocate = " << to_allocate);
    bipc::shared_memory_object::remove(parameters_.shm_name.c_str());
    segment_ = new bipc::managed_shared_memory(bipc::create_only, parameters_.shm_name.c_str(), to_allocate);

    grid2_ = segment_->construct<FixedGrid2f>("fixedgrid2f")(parameters_.origin,
                                                             parameters_.dimension,
                                                             parameters_.resolution);
    grid_data_ = segment_->construct<SemanticGridCell>("semantic_data")[grid2_->num_cells()]();

    grid_data_wrapper_.reset(grid2_->dimension(), grid_data_);

    init_semaphore_->post();
    float p[4];
    p[0]=p[1]=p[2]=0.1; p[3] = 0.7;
    InitializeGrid(0, 1, p, lfm, dem);
    return true;
  }

  virtual ~SharedSemanticMap() {
    namespace bipc = boost::interprocess;
    delete segment_;
    bipc::shared_memory_object::remove(parameters_.shm_name.c_str());
    init_semaphore_->remove(init_grid_semaphore_name_.c_str());
  }

  virtual void InitializeGrid(double height_mean, double height_uncertainty, float *p, LocalFrameManager *lfm = NULL, DEMData *dem = NULL){
    if(lfm == NULL|| dem == NULL || !lfm->IsInitialized()){
        for(int i = 0; i< grid2_->dim_i(); i++){
            for(int j = 0; j< grid2_->dim_j(); j++){
                SemanticGridCell& g = grid_data_[grid2_->grid_to_mem(i,j)];
                g.height_mean_ = height_mean;
                g.height_uncertainty_ = height_uncertainty;
                g.p_classes_[(int)ca::SemanticClass::building] = 0.01;
                g.p_classes_[(int)ca::SemanticClass::car] = 0.01;
                g.p_classes_[(int)ca::SemanticClass::ground_dirt] = 0.6;
                g.p_classes_[(int)ca::SemanticClass::ground_grass] = 0.1;
                g.p_classes_[(int)ca::SemanticClass::high_vegetation] = 0.1;
                g.p_classes_[(int)ca::SemanticClass::low_vegetation] = 0.1;
            }
        }
    }
    else{
        for(int i = 0; i< grid2_->dim_i(); i++){
            for(int j = 0; j< grid2_->dim_j(); j++){
                SemanticGridCell& g = grid_data_[grid2_->grid_to_mem(i,j)];
                FixedGrid2f::Vec2 v = grid2_->grid_to_world(i,j); // This assumes that there is an offset between grid frame and static local frame
                Eigen::Vector3d pos(v.x(),v.y(),0.0);
                double lat,lon,alt;
                if(!lfm->TransformLocalFrame2LatLon(pos,grid_frame_,lat,lon,alt)){
                    ROS_ERROR_STREAM("Could not convert from semantic grid frame::"<<grid_frame_<<" to lat lon.");
                    ROS_ERROR_STREAM("Semantic Grid will now shutdown as it cannot initialize properly.");
                    exit(-1);
                }
                lat = lat*RAD2DEG_C;
                lon = lon*RAD2DEG_C;
                bool valid = false;
                alt = dem->getHeight(lat,lon,&valid);
                if(!valid){
                    ROS_ERROR_STREAM("Could not dem height for::"<<lat<<"::"<<lon);
                    ROS_ERROR_STREAM("Semantic Grid will now shutdown as it cannot initialize properly.");
                    exit(-1);
                }

                if( !lfm->MSL2Altitude(alt,alt) ||
                    !lfm->TransformLatLon2LocalFrame(lat*DEG2RAD_C,lon*DEG2RAD_C,alt,pos,grid_frame_)){
                    ROS_ERROR_STREAM("Could not convert from lat lon to semantic grid frame.");
                    ROS_ERROR_STREAM("Semantic Grid will now shutdown as it cannot initialize properly.");
                    exit(-1);
                }
                g.height_mean_ = pos.z()+ 1.0;
                g.height_uncertainty_ = height_uncertainty;
                g.p_classes_[(int)ca::SemanticClass::building] = 0.01;
                g.p_classes_[(int)ca::SemanticClass::car] = 0.01;
                g.p_classes_[(int)ca::SemanticClass::ground_dirt] = 0.6;
                g.p_classes_[(int)ca::SemanticClass::ground_grass] = 0.1;
                g.p_classes_[(int)ca::SemanticClass::high_vegetation] = 0.1;
                g.p_classes_[(int)ca::SemanticClass::low_vegetation] = 0.1;
            }
        }
    }
  }

  ca::DenseArray2<SemanticGridCell>& GetGridDataWrapper(){ return grid_data_wrapper_;}
  ca::FixedGrid2f* GetFixedGrid(){ return grid2_;}
  ca::SemanticGridCell* GetMem(){ return grid_data_;}
  void set_grid_frame(std::string frame_name){grid_frame_ = frame_name;}
  std::string get_grid_frame(){ return grid_frame_;}
private:
  SemanticMapParameters parameters_;
  boost::interprocess::managed_shared_memory* segment_;
  boost::interprocess::named_semaphore* init_semaphore_;
  std::string init_grid_semaphore_name_;
  ca::FixedGrid2f* grid2_;
  ca::DenseArray2<SemanticGridCell> grid_data_wrapper_;
  SemanticGridCell* grid_data_;
  std::string grid_frame_;
private:
  SharedSemanticMap(const SharedSemanticMap& other);
  SharedSemanticMap& operator=(const SharedSemanticMap& other);
};

} /* ca */

#endif /* end of include guard: SHARED_SEMANTICMAP_HPP_9FWOQEMW */
// ray function -- done
// make sure ray casting is in order --done
// attach functor -- done
// initialization function --done
// write the outer loop with camera and transfomations
// ---------------------------------------------------------
// add DEM
