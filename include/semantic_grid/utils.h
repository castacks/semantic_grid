#ifndef UTILS_H
#define UTILS_H
#include "opencv2/core/core.hpp"
#include <geom_cast_extra/geom_cast_extra.h>
#include <Eigen/Dense>
#include
Eigen::Vector3d CameraCoordinate2Ray(unsigned int x, unsigned int y, cv::Mat camera_intrinsics);
#endif // UTILS_H
