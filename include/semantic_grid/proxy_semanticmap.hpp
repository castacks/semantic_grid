#ifndef PROXY_SEMANTICMAP_HPP_JPCTAMDR
#define PROXY_SEMANTICMAP_HPP_JPCTAMDR

#include <ros/ros.h>
#include <ros/console.h>

#include <boost/interprocess/managed_shared_memory.hpp>

#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/named_semaphore.hpp>
#include <scrollgrid/fixedgrid2.hpp>
#include <scrollgrid/dense_array2.hpp>

#include <semantic_grid/semantic_map_datatypes.h>

namespace ca
{

class ProxySemanticMap {

  public:
     ProxySemanticMap():
      shared_memory_identifier_("shared_semantic_map"),
      gridmap_initialized_(false),
      read_write_lock_(false) {
        /*namespace bipc=boost::interprocess;
	init_grid_semaphore_name_= "init_grid_semaphore";
	shared_grid_semaphore_name_ = "shared_grid_semaphore";
        init_semaphore_= new bipc::named_semaphore(bipc::open_or_create,init_grid_semaphore_name_.c_str(),0);*/
    }   
    ProxySemanticMap(std::string grid_identifier):
      shared_memory_identifier_(grid_identifier),
      gridmap_initialized_(false),
      read_write_lock_(false) {}

    virtual ~ProxySemanticMap() {
      delete segment_;
      semaphore_->remove(shared_grid_semaphore_name_.c_str());
      init_semaphore_->remove(init_grid_semaphore_name_.c_str());
    }

    bool init() {
      namespace bipc = boost::interprocess;
      init_grid_semaphore_name_= shared_memory_identifier_+"_init_semantic_semaphore";
      shared_grid_semaphore_name_ = shared_memory_identifier_+"_shared_semantic_semaphore";
      init_semaphore_= new bipc::named_semaphore(bipc::open_or_create,init_grid_semaphore_name_.c_str(),0);

      init_semaphore_->wait();
      ROS_INFO("grid creation is detected");
      //TODO access control options
      segment_ = new bipc::managed_shared_memory(bipc::open_only, shared_memory_identifier_.c_str());
      {
        std::pair<ca::FixedGrid2f*, size_t> result;
        result = segment_->find<FixedGrid2f>("fixedgrid2f");
        // TODO error checking
        grid2_ = result.first;

      }
      {
        std::pair<SemanticCell*, size_t> result;
        // TODO error checking
        result = segment_->find<SemanticCell>("grid_data");
        grid_data_ = result.first;
      }
      //ROS_INFO_STREAM("grid3_->num_cells() = " << grid3_->num_cells());
      grid_data_wrapper_.reset(grid2_->dimension(), grid_data_);

      //Named semaphore initialization for synchronization
      semaphore_ = new bipc::named_semaphore(bipc::open_or_create, shared_grid_semaphore_name_.c_str(),1);

      ROS_INFO("grid inititalized");
      init_semaphore_->post();
      return true;
    }

    void FreeWriteLock() {
      semaphore_->post();
    }

    void WaitForWriteLock() {
      semaphore_->wait();
    }

    // TODO synchronization
    ca::SemanticCell get(grid_ix_t i, grid_ix_t j, Eigen::Matrix<float, 2, 1>& grid_cell_in_world) {
      ca::Vec2Ix X(i,j);
      //WaitForWriteLock(); 
      namespace bipc = boost::interprocess;
      ca::SemanticCell grid_cell;
      {
        if(!grid2_->is_inside_grid(X))
        {
            grid_ix_t mem_ix = grid2_->grid_to_mem(X);
            grid_cell = grid_data_wrapper_[mem_ix];
            grid_cell_in_world = grid2_ ->grid_to_world(X);
        }
      }
      //FreeWriteLock();
      return grid_cell;
    }

    void set(grid_ix_t i, grid_ix_t j, ca::SemanticCell& val) {
      namespace bipc = boost::interprocess;
      {
//        WaitForWriteLock();
        grid_ix_t mem_ix = grid2_->grid_to_mem(i, j);
        grid_data_wrapper_[mem_ix] = val;
//        FreeWriteLock();
      }
    }

    const ca::FixedGrid2f* GetGrid() const { return grid2_; }
    const ca::DenseArray2<SemanticCell>& GetStorage() const { return grid_data_wrapper_; }

    ca::FixedGrid2f* GetGrid() { return grid2_; }
    ca::DenseArray2<SemanticCell>& GetStorage() { return grid_data_wrapper_; }

    //forwarded functions from scrollgrid (when we dont need the whole grid, just a little info
    const Vec2Ix& dimension() const { return grid2_->dimension(); }
    const Vec2Ix& scrolloffset() const {return grid2_->scroll_offset();}
    const ca::scrollgrid::Box<float,2>& box() const { return grid2_->box(); }

  private:
    boost::interprocess::managed_shared_memory* segment_;
    std::string shared_memory_identifier_,shared_grid_semaphore_name_,init_grid_semaphore_name_;
    ca::FixedGrid2f* grid2_;
    ca::DenseArray2<SemanticCell> grid_data_wrapper_;
    SemanticCell* grid_data_;

    bool lock;

  private:
    ProxySemanticMap(const ProxyGridMap& other);
    ProxySemanticMap& operator=(const ProxyGridMap& other);

    boost::interprocess::named_semaphore* semaphore_;
    bool read_write_lock_;

    //initialization conditions
    boost::interprocess::named_semaphore* init_semaphore_;
    bool gridmap_initialized_;
};

} /* ca */

#endif /* end of include guard: PROXY_GRIDMAP_HPP_JPCTAMDR */
