#ifndef PROBABILITY_TOOLS_H
#define PROBABILITY_TOOLS_H
#include <cmath>

namespace probability_tools{
   inline double UpdateLogOddsUsingProbability(double prior_logodds, double p_z_given_a, double p_z_given_nota){
    double updated_val =  (prior_logodds + std::log(p_z_given_a/p_z_given_nota));
    if(std::isinf(updated_val)){
        if(p_z_given_a < 0.5)
            updated_val = -30.0;
        else
            updated_val = 30.0;
    }
    return updated_val;
   }

   inline double UpdateLogOddsUsingLogOdds(double prior_logodds, double measurement_logodds){
    double updated_val =  prior_logodds + measurement_logodds;
    return updated_val;
   }

   inline double P2LogOdds(double p){
        double value = std::log(p/1-p);
        if(std::isinf(value)){
            if(p < 0.5)
                value = -30.0;
            else
                value = 30.0;
        }
        return value;
   }

   inline double LogOdds2P(double log_odds){
        double value = std::pow(2,log_odds);
        value = value/(1+value);
        if(std::isnan(value)){
            if(log_odds >0)
                value = 1.0;
            else
                value =0.0;
        }
        return value;
   }
#endif // PROBABILITY_TOOLS_H
}
