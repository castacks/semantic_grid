#ifndef SIMPLE_PLANNER_H
#define SIMPLE_PLANNER_H
//#include "ros/ros.h"
#include "semantic_grid/camera_interface.h"
#include "visualization_msgs/Marker.h"
#include "semantic_grid/shared_semanticmap.hpp"
#include "ca_nav_msgs/PathXYZVViewPoint.h"

class SimplePlanner
{
    double radius_;
    double movement_threshold_;
    Eigen::Vector3d last_car_location_;
    double height_;
    double speed_;
    double minimum_detection_distance_;
    unsigned int number_waypoints_around_car_;
    int car_threshold_;
    bool viewpoint_active_;
    bool IsVisited(std::vector<size_t> visited_list, size_t mem_id, size_t & mem_location);

public:
    bool SetParams(ros::NodeHandle &n);
    Eigen::Vector3d GetCarLocation(){ return last_car_location_;}
    void FloodFillCarFinder(size_t i, size_t j, ca::FixedGrid2f* grid, ca::SemanticGridCell* grid_data, ca::SemanticClass cc, Eigen::Vector3d &v, unsigned int &count);
    Eigen::Vector3d DetectCar(ca::FixedGrid2f* grid, ca::SemanticGridCell* grid_data, ca::SemanticClass cc, bool &found_car);
    ca_nav_msgs::PathXYZVViewPoint MakePath(ca::FixedGrid2f* grid, ca::SemanticGridCell* grid_data, std::string frame_id, Eigen::Vector3d pos, ca::SemanticClass cc);
    visualization_msgs::Marker MakePathMarker(ca_nav_msgs::PathXYZVViewPoint &path);
    visualization_msgs::Marker CarMarker(Eigen::Vector3d car_location, std::string frame_id);
    SimplePlanner();
};

#endif // SIMPLE_PLANNER_H
