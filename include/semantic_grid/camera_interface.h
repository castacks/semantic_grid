#ifndef SEMANTIC_GRID_CAMERA_INTERFACE_H
#define SEMANTIC_GRID_CAMERA_INTERFACE_H
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv/cv.h>
#include <cv_bridge/cv_bridge.h>
#include <image_geometry/pinhole_camera_model.h>
#include <sensor_msgs/image_encodings.h>
#include <geom_cast_extra/geom_cast_extra.h>
#include "semantic_segmentation_msgs/LabelsTensor.h"

class CameraInterface
{
protected:
    image_transport::ImageTransport it_;
    image_transport::CameraSubscriber sub_;

public:
    cv::Mat image_;
    image_geometry::PinholeCameraModel cam_model_;
    bool Initialized(){return cam_model_.initialized();}
    uchar getClassAt(size_t x, size_t y);
    Eigen::Vector3d getRayAt(double x, double y);
    CameraInterface(ros::NodeHandle &nh);
    virtual void Initialize(std::string image_topic);
    virtual void ImageCb(const sensor_msgs::ImageConstPtr& image_msg,
                 const sensor_msgs::CameraInfoConstPtr& info_msg);

};

class SemanticCameraInterface : public CameraInterface
{
public:
    uint8_t RGB2Label(int ip);
    void Image2Labels(cv::Mat & ip_image, cv::Mat & op_image);
    virtual void Initialize(std::string image_topic);
    virtual void ImageCb(const sensor_msgs::ImageConstPtr& image_msg,
                 const sensor_msgs::CameraInfoConstPtr& info_msg);
    SemanticCameraInterface(ros::NodeHandle &nh);
};

class LabelTensorInterface{
public:
    std::vector<uint8_t> labels_data_;
    std_msgs::MultiArrayLayout labels_data_layout_;
    ros::Subscriber label_tensor_sub_;
    image_geometry::PinholeCameraModel cam_model_;
    Eigen::Vector3d getRayAt(double x,double y);
    uint8_t getClassProbability(size_t x, size_t y, size_t c, bool &valid);
    size_t getNumLabels();
    void LabelsTensorSub(const semantic_segmentation_msgs::LabelsTensorConstPtr& label_msg);
    bool Initialized(){
        return cam_model_.initialized();
    }
};
#endif
