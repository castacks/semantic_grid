#ifndef RGBD_GRID_SENSOR_HPP_I9SAOOSJ
#define RGBD_GRID_SENSOR_HPP_I9SAOOSJ

#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <tr1/unordered_map>
#include <tr1/memory>

#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>

#include <pcl_ros/point_cloud.h>
#include <pcl/common/transforms.h>

#include <tf_utils/tf_utils.h>
#include <tictoc_profiler/profiler.hpp>
#include <gridmapping2/proxy_gridmap.hpp>
#include <gridmapping_msgs/GridPoint.h>
#include <gridmapping_msgs/GridObstacleUpdate.h>

#include <scrollgrid/scrollgrid3.hpp>
#include <scrollgrid/raycasting.hpp>
#include <scrollgrid/occ_raycasting.hpp>
#include <scrollgrid/scrolling_strategies.hpp>
#include <scrollgrid/grid_util.hpp>

namespace ca {

// TODO why is this class named like this anyway?
// It is not a sensor.
// Possible name: RgbdGridMapping
class RGBDGridSensor {
 public:

  typedef pcl::PointCloud<pcl::PointXYZ> PointCloudXYZ;

  //Parameters specific for rgbd grid
  static const uint8_t CA_SG_UNKNOWN;
  static const int32_t CA_SG_COMPLETELY_FREE;
  static const int32_t CA_SG_COMPLETELY_OCCUPIED;
  static const int32_t CA_SG_BARELY_FREE;
  static const int32_t CA_SG_BARELY_OCCUPIED;
  static const int32_t CA_SG_BELIEF_UPDATE_POS; // when hit
  static const int32_t CA_SG_BELIEF_UPDATE_NEG;

 public:

  RGBDGridSensor(ros::NodeHandle& n):
      initialized_(false),
      counter(0) {
        n.param<std::string>("/rgbd_grid_sensor/pointCloudFrame", point_cloud_frame_, "/camera_rgb_optical_frame");
        n.param<std::string>("/rgbd_grid_sensor/worldFrame", world_frame_, "/world_frame");
        n.param<std::string>("/rgbd_grid_sensor/sharedGridIdentifer",shared_grid_identifier_,"shared_grid_map");
        grid_obst_update_pub_=n.advertise<gridmapping_msgs::GridObstacleUpdate>("/rgbd_grid_sensor/obstacleupdates",10);
	proxy_=new ca::ProxyGridMap(shared_grid_identifier_);
        ROS_INFO_STREAM("rgbd grid sensor :"<<point_cloud_frame_<<" , "<<world_frame_);
      }

  virtual ~RGBDGridSensor() {
   delete proxy_;
  }

 public:

  void AddPointCloud(const PointCloudXYZ::ConstPtr &msg);
  void PublishObstacleUpdates();//const ros::Time& timestamp);
  void CompactObstacleUpdates();
  void ScrollGrid();
  void ClearGrid(const ca::Vec3Ix& start, const ca::Vec3Ix& finish);
  void RayCasting();
  void Init();

  void AddCube() {
    //For debugging
    ROS_INFO("add cube");
    for (int i=0;i<10;i++)for(int j=0;j<10;j++)for(int k=0;k<10;k++){
      proxy_->set(i,j,k,255);
    }
  }

  bool UpdateProbability(int x,int y,int z, bool endpt){
    //      ca::Profiler::tictoc("rgbd_grid_sensor::upprob");
    bool isadd=false,isremove=false;
    if(!grid3_->is_inside_grid(x,y,z))ROS_INFO_STREAM("updateprob:isinsideGrid"<<x<<" , "<<y<<" , "<<z);
    mem_ix_t mem_ix = grid3_->grid_to_mem(x, y, z);
    int32_t old_value = occ_array3_[mem_ix];
    if(!endpt){
      int32_t new_value = static_cast<int32_t>(occ_array3_[mem_ix])-CA_SG_BELIEF_UPDATE_NEG;
      new_value=std::max(CA_SG_COMPLETELY_FREE, new_value);
      occ_array3_[mem_ix] = static_cast<uint8_t>(new_value);
      isremove = (old_value>=CA_SG_BARELY_FREE && new_value<=CA_SG_BARELY_FREE); //REMOVE point from list
    }
    else{
      int32_t new_value = static_cast<int32_t>(occ_array3_[mem_ix])+CA_SG_BELIEF_UPDATE_POS;
      new_value=std::min(CA_SG_COMPLETELY_OCCUPIED, new_value);
      occ_array3_[mem_ix] = static_cast<uint8_t>(new_value);
      isadd = (old_value<CA_SG_BARELY_FREE && new_value>=CA_SG_BARELY_OCCUPIED);  //ADD point from list
    }
    if(isadd||isremove){
      gridmapping_msgs::GridPoint cell;
      cell.x = x;
      cell.y = y;
      cell.z = z;
      obstlist.cells.push_back(cell);
      if(isadd) obstlist.states.push_back(1);
      if(isremove) obstlist.states.push_back(0);
    }
    //      ca::Profiler::tictoc("rgbd_grid_sensor::upprob");
  }

 private:

  std::string point_cloud_frame_;
  std::string world_frame_;
  std::string shared_grid_identifier_;

  ca::ProxyGridMap* proxy_;
  ca::ScrollGrid3f* grid3_;
  ca::DenseArray3<uint8_t> occ_array3_;
  int counter;
  ca::ScrollGrid3f::Vec3 sensor_pos_;

  //Sending pbstacle updates
  gridmapping_msgs::GridObstacleUpdate obstlist;
  ros::Publisher grid_obst_update_pub_;

  //Scrolling
  ca::ScrollForBaseFrame<float> scroll_strategy_;
  ca::Vec3Ix scroll_cells_;
  ca::Vec3Ix clear_i_min, clear_i_max, clear_j_min, clear_j_max, clear_k_min, clear_k_max;

  //Raycasting
  ca::ScrollGrid3f::Vec3 ray_end_pos_;
  ca::Vec3Ix sensor_pos_ijk_;
  ca::Vec3Ix ray_end_pos_ijk_;

  //Input point cloud is provided in local frame and therefore converted to point_cloud_global
  //If point cloud is global, assign it directly to point_cloud_global //TODO
  tf::TransformListener tf_listener_;
  PointCloudXYZ point_cloud_global_;
  tf::StampedTransform w_to_sensor_transform_;

  bool initialized_;

};

}
#endif
