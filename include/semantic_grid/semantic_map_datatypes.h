#ifndef SEMANTIC_MAP_DATATYPES_H
#define SEMANTIC_MAP_DATATYPES_H
#include <scrollgrid/fixedgrid2.hpp>
#include <scrollgrid/dense_array2.hpp>
#include <semantic_grid/probability_tools.h>

namespace ca
{


struct SemanticMapParameters {
  SemanticMapParameters() {
    shm_name = "shared_semantic_map";
    dimension = ca::Vec2Ix(50,90);
    resolution = 5;
    origin.x() = 0.0;//(float)0.5*dimension.x()*resolution;
    origin.y() = 0.0;//(float)0.5*dimension.y()*resolution;
  }
  SemanticMapParameters(std::string name, ca::Vec2Ix dim, float res) {
    shm_name = name;
    dimension = dim;
    resolution = res;
  }
  std::string shm_name;
  ca::Vec2Ix dimension;
  Eigen::Vector2f origin;
  float resolution;
};

enum class SemanticClass{
    building = 0,
    car,
    ground_dirt,
    ground_grass,
    high_vegetation,
    low_vegetation
};

class ClassProperties{
public:
    double upper_height_bound_;
    double lower_height_bound_;
    std::vector<double> possible_heights_;
    double p_obs_for_;
    double p_obs_against_;
    int num_obs_for_;
    int num_obs_against_;
    bool  height_initialized_;
    double ReportHeight(){
        double return_height = possible_heights_[0];
        if(upper_height_bound_ == -std::pow(10,6) || !height_initialized_)
            return return_height;
        else{
            return_height = upper_height_bound_;
            double error_height = std::pow(10,6);
            for(size_t i=0; i<possible_heights_.size(); i++){
                double temp_err_height = std::abs(return_height-possible_heights_[i]);
                if(temp_err_height < error_height)
                {
                    error_height = temp_err_height;
                    return_height = possible_heights_[i];
                }
            }
        }
        return return_height;
    }

    ClassProperties(){
        height_initialized_ = false;
        upper_height_bound_ = -std::pow(10,6);
        lower_height_bound_ = std::pow(10,6);
        p_obs_for_ = 0.0;
        p_obs_against_ = 0.0;
        num_obs_for_ = 0;
        num_obs_against_ = 0;
    }
};


class SemanticGridCell{
public:
    std::vector<int> integrated_logodds_;
    std::vector<double> p_classes_;
    std::vector<ClassProperties> class_properties_;
    bool casted_;

    double min_observation_distance_;
    double height_mean_;
    double height_uncertainty_;
    uint8_t DetectCaseForHeightObservationModel(SemanticClass c);
    bool UpdateClassProperties(SemanticClass c, double height, double distance, double p);
    void UpdateClassProbabilities(SemanticClass c, double p, double distance);
    bool ObservationModel(SemanticClass a_c, double* p_z_given_a, double* p_z_given_nota, double distance, SemanticClass z_c, double p);
    bool HeightBasedObservationModel();
    bool NumRaysBasedObservationModel();
    void UpdateIntegratedLogOdds();
    double P_Z_Given_A(double distance, SemanticClass z_c, SemanticClass a_c);
    double P_Z_Given_NOTA(double distance, SemanticClass z_c, SemanticClass a_c);
    SemanticGridCell();
};

typedef FixedGrid2<float> FixedGrid2f;

}
#endif // SEMANTIC_MAP_DATATYPES_H
