#include "ros/ros.h"
#include "semantic_grid/camera_interface.h"
#include "visualization_msgs/Marker.h"

visualization_msgs::Marker InitializeMarker(std::string frame){
    visualization_msgs::Marker marker;
    marker.header.frame_id = frame;
    marker.header.stamp = ros::Time();
    marker.ns = "rays";
    marker.frame_locked = true;
    marker.id = 0;
    marker.type = visualization_msgs::Marker::LINE_LIST;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = 0.0;
    marker.pose.position.y = 0.0;
    marker.pose.position.z = 0.0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 0.1;
    marker.scale.y = 0.1;
    marker.scale.z = 0.1;
    marker.color.a = 0.01; // Don't forget to set the alpha!
    marker.color.r = 1.0;
    marker.color.g = 1.0;
    marker.color.b = 1.0;
    return marker;
}

void AddRay(visualization_msgs::Marker& m, Eigen::Vector3d& ray)
{
    geometry_msgs::Point p; p.x=0; p.y=0; p.z=0;
    geometry_msgs::Point p1; p1.x=ray.x(); p1.y=ray.y(); p1.z=ray.z();
    m.points.push_back(p); m.points.push_back(p1);
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "listener");
  ros::NodeHandle n;
  std::string image_topic= "/front_cam_left/camera_left/image";
  ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>( "/testing_camera_interface/camera_rays", 0 );
  CameraInterface cmi(n);
  cmi.Initialize(image_topic);
  ros::Rate r(10); // 10 hz

  while(ros::ok()){
    if(cmi.Initialized()){
        visualization_msgs::Marker m = InitializeMarker(cmi.cam_model_.tfFrame());
        for(size_t i=0; i<cmi.image_.rows;i++){
            for(size_t j=0; j<cmi.image_.cols;j++){
                Eigen::Vector3d v = 10*cmi.getRayAt(i,j);
                AddRay(m,v);
            }
        }
        marker_pub.publish(m);
    }

    ros::spinOnce();
    r.sleep();
  }
  return 0;
}
