#include "semantic_grid/simple_planner.h"

bool SimplePlanner::IsVisited(std::vector<size_t> visited_list, size_t mem_id, size_t & mem_location){
    for(size_t i=0; i<visited_list.size(); i++){
        if(mem_id == visited_list[i]){
            mem_location = i;
            return true;
        }
    }
    return false;
}

ca_nav_msgs::PathXYZVViewPoint SimplePlanner::MakePath(ca::FixedGrid2f* grid, ca::SemanticGridCell* grid_data, std::string frame_id, Eigen::Vector3d pos, ca::SemanticClass cc){

    ca_nav_msgs::PathXYZVViewPoint path; // lets make the path
    Eigen::Vector3d car_pos;

    double detection_distance = (pos - last_car_location_).norm();
    if(detection_distance < minimum_detection_distance_){ // If we are close to the car, stop changing its location
        car_pos = last_car_location_;
        return path;
    }
    else{
        bool found_car = false;
        car_pos = DetectCar(grid, grid_data,cc,found_car);
        if(!found_car){
            ROS_ERROR_STREAM("Semantic Grid::No car found.");
            return path;
        }

        double car_movement = (car_pos - last_car_location_).norm();
        if(car_movement < movement_threshold_){ // If the car didnt move too much, it didnt move at all
            car_pos = last_car_location_;
            return path;
        }

    }
    last_car_location_ = car_pos;
    car_pos.z() = car_pos.z() - height_; // Assuming NED frames

    Eigen::Vector3d v = pos - car_pos; v.z() =0 ;v.normalize();
    Eigen::Vector3d second_wp = car_pos + v*radius_ ; //first wp is pos

    double theta = std::atan2(v.y(),v.x());
    double del_theta = M_2PI/number_waypoints_around_car_;

    ca_nav_msgs::XYZVViewPoint msg_wp;

//    msg_wp.position.x = pos.x();
//    msg_wp.position.y = pos.y();
//    msg_wp.position.z = pos.z();
    msg_wp.vel = speed_;
    msg_wp.safety = true;
    msg_wp.viewpoint.x = car_pos.x();
    msg_wp.viewpoint.y = car_pos.y();
    msg_wp.viewpoint.z = car_pos.z();
    //path.waypoints.push_back(msg_wp);

    msg_wp.position.x = second_wp.x();
    msg_wp.position.y = second_wp.y();
    msg_wp.position.z = second_wp.z();
    path.waypoints.push_back(msg_wp);

    msg_wp.safety = !viewpoint_active_;

    for(int i=1; i<= number_waypoints_around_car_; i++){
        Eigen::Vector3d wp = radius_*Eigen::Vector3d
                                        (std::cos(i*del_theta+theta), std::sin(i*del_theta+theta),0);
        wp = car_pos + wp;
        msg_wp.position.x = wp.x();
        msg_wp.position.y = wp.y();
        msg_wp.position.z = wp.z();
        path.waypoints.push_back(msg_wp);
    }

    path.header.stamp = ros::Time::now();
    path.header.frame_id = frame_id;
    return path;
}



void SimplePlanner::FloodFillCarFinder( size_t i, size_t j, ca::FixedGrid2f* grid, ca::SemanticGridCell* grid_data, ca::SemanticClass cc,Eigen::Vector3d& v, unsigned int &count)
{
    std::vector<size_t> open_list;
    std::vector<size_t> visited_list;

    //semantic class not found
    if(i >= grid->dim_i() || j >= grid->dim_j())
        return;

    size_t mem_id = grid->grid_to_mem(i,j);
    open_list.push_back(mem_id);

    size_t dummy;

    for(size_t i=0; i< open_list.size(); i++){
        ca::SemanticGridCell c = grid_data[open_list[i]];
        int max_id = 0;
        float p_max = 0.0;
        if(c.integrated_logodds_[(int)cc] > car_threshold_){
            count++;
            ca::Vec2Ix grid_v = grid->mem_to_grid(open_list[i]);
            ca::FixedGrid2f::Vec2 vv = grid->grid_to_world(grid_v);
            v = v + Eigen::Vector3d(vv.x(),vv.y(),c.height_mean_);
            //check pixel above
            if((grid_v.x()+1)<grid->dim_i() && !IsVisited(visited_list,grid->grid_to_mem(grid_v.x()+1,grid_v.y()),dummy))
                open_list.push_back(grid->grid_to_mem(grid_v.x()+1,grid_v.y()));

            //check pixel below
            if(grid_v.x()>0 && !IsVisited(visited_list,grid->grid_to_mem(grid_v.x()-1,grid_v.y()),dummy))
                open_list.push_back(grid->grid_to_mem(grid_v.x()-1,grid_v.y()));

            //check pixel to right
            if((grid_v.y()+1)<grid->dim_j() && !IsVisited(visited_list,grid->grid_to_mem(grid_v.x(),grid_v.y()+1),dummy))
                open_list.push_back(grid->grid_to_mem(grid_v.x(),grid_v.y()+1));

            //check pixel to left
            if(grid_v.y()>0 && !IsVisited(visited_list,grid->grid_to_mem(grid_v.x(),grid_v.y()-1),dummy))
                open_list.push_back(grid->grid_to_mem(grid_v.x(),grid_v.y()-1));

        }
        visited_list.push_back(open_list[i]);
    }
    return;
}

Eigen::Vector3d SimplePlanner::DetectCar(ca::FixedGrid2f* grid, ca::SemanticGridCell* grid_data, ca::SemanticClass cc, bool& found_car){

    for(size_t i=0; i < grid->dim_i(); i++){ // Finding the first cell of the car
        for(size_t j=0; j < grid->dim_j(); j++){
            ca::SemanticGridCell& c = grid_data[grid->grid_to_mem(i,j)];
            if(c.integrated_logodds_[(int)cc] > car_threshold_){
                //ROS_INFO_STREAM("Found a car!");
                unsigned int count = 0;
                Eigen::Vector3d vv(0,0,0);
                FloodFillCarFinder(i,j,grid,grid_data,cc,vv,count);
                vv = vv/count;
                found_car = true;
                return vv;
                break;
            }
        }
    }

    found_car = false;
    return Eigen::Vector3d(0,0,0);
}

visualization_msgs::Marker SimplePlanner::MakePathMarker(ca_nav_msgs::PathXYZVViewPoint &path){
    visualization_msgs::Marker m;
    m.header.frame_id = path.header.frame_id;
    m.header.stamp = ros::Time();
    m.ns = "exploration_path";
    m.frame_locked = true;
    m.id = 0;
    m.type = visualization_msgs::Marker::LINE_STRIP;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = 0.0;
    m.pose.position.y = 0.0;
    m.pose.position.z = 0.0;
    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 1;
    m.scale.y = 1;
    m.scale.z = 1;
    m.color.a = 1.0; // Don't forget to set the alpha!
    m.color.r = 1.0;
    m.color.g = 0.0;
    m.color.b = 0.0;
    geometry_msgs::Point p;
    for(size_t i=0; i<path.waypoints.size();i++){
        p.x = path.waypoints[i].position.x;
        p.y = path.waypoints[i].position.y;
        p.z = path.waypoints[i].position.z;
        m.points.push_back(p);
    }

    return m;
}

visualization_msgs::Marker SimplePlanner::CarMarker(Eigen::Vector3d car_location, std::string frame_id){
    visualization_msgs::Marker m;
    m.header.frame_id = frame_id;
    m.header.stamp = ros::Time();
    m.ns = "detected_car_location";
    m.frame_locked = true;
    m.id = 0;
    m.type = visualization_msgs::Marker::CUBE;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = car_location.x();
    m.pose.position.y = car_location.y();
    m.pose.position.z = car_location.z();
    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 3;
    m.scale.y = 3;
    m.scale.z = 3;
    m.color.a = 0.7; // Don't forget to set the alpha!
    m.color.r = 1.0;
    m.color.g = 0.0;
    return m;
}

bool SimplePlanner::SetParams(ros::NodeHandle &n){
    std::string parameter;
    parameter = "/simple_planner/radius_around_car";
    if (!n.getParam(parameter, radius_)){
      ROS_ERROR_STREAM("Could not get simple planner param::"<<parameter);
      return false;
    }

    parameter = "/simple_planner/car_movement_threshold";
    if (!n.getParam(parameter, movement_threshold_)){
      ROS_ERROR_STREAM("Could not get simple planner param::"<<parameter);
      return false;
    }

    parameter = "/simple_planner/car_height_offset";
    if (!n.getParam(parameter, height_)){
      ROS_ERROR_STREAM("Could not get simple planner param::"<<parameter);
      return false;
    }

    parameter = "/simple_planner/speed";
    if (!n.getParam(parameter, speed_)){
      ROS_ERROR_STREAM("Could not get simple planner param::"<<parameter);
      return false;
    }

    parameter = "/simple_planner/minimum_update_distance";
    if(!n.getParam(parameter, minimum_detection_distance_)){
      ROS_ERROR_STREAM("Could not get simple planner param::"<<parameter);
      return false;
    }

    parameter = "/simple_planner/number_waypoints_around_car";
    int num_wp;
    if(!n.getParam(parameter, num_wp)){
        num_wp = number_waypoints_around_car_;
      ROS_ERROR_STREAM("Could not get simple planner param::"<<parameter);
      return false;
    }

    parameter = "/simple_planner/car_threshold";
    if(!n.getParam(parameter, car_threshold_)){
      ROS_ERROR_STREAM("Could not get simple planner param::"<<parameter);
      return false;
    }

    parameter = "/simple_planner/viewpoint_active";
    if(!n.getParam(parameter, viewpoint_active_)){
      ROS_ERROR_STREAM("Could not get simple planner param::"<<parameter);
      return false;
    }

    return true;
}

SimplePlanner::SimplePlanner(){
    radius_ = 7;
    movement_threshold_ = 3;
    height_ = 3;
    speed_ = 2;
    minimum_detection_distance_ = 20;
    number_waypoints_around_car_ = 6;
    last_car_location_ = Eigen::Vector3d(0,0,0);
    car_threshold_ = 200;
    viewpoint_active_ = false;
    //car_found_ = false;
}
