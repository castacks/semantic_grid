#include "semantic_grid/camera_interface.h"

uchar CameraInterface::getClassAt(size_t x, size_t y){
    return image_.at<uchar>(cv::Point(x,y));
}

Eigen::Vector3d CameraInterface::getRayAt(double x, double y){
    cv::Point2d px_cv(x,y);
    cv::Point3d pt_cv = cam_model_.projectPixelTo3dRay(px_cv);
    Eigen::Vector3d v = ca::point_cast<Eigen::Vector3d>(pt_cv);
    v.normalize();
    return v;
}

CameraInterface::CameraInterface(ros::NodeHandle &nh)
    : it_(nh)
{}

void CameraInterface::Initialize(std::string image_topic)
{
  sub_ = it_.subscribeCamera(image_topic, 1, &CameraInterface::ImageCb, this);
}

void CameraInterface::ImageCb(const sensor_msgs::ImageConstPtr& image_msg,
             const sensor_msgs::CameraInfoConstPtr& info_msg){
    cv_bridge::CvImagePtr input_bridge;
    try {
        input_bridge = cv_bridge::toCvCopy(image_msg, sensor_msgs::image_encodings::TYPE_32SC1);
        image_ = input_bridge->image;

    }
    catch (cv_bridge::Exception& ex){
    ROS_ERROR("[CameraInterface] Failed to convert image");
    return;
    }

    cam_model_.fromCameraInfo(info_msg);
}


void SemanticCameraInterface::ImageCb(const sensor_msgs::ImageConstPtr& image_msg,
             const sensor_msgs::CameraInfoConstPtr& info_msg){
    cv_bridge::CvImagePtr input_bridge;
    cv::Mat image;
    try {
        input_bridge = cv_bridge::toCvCopy(image_msg, sensor_msgs::image_encodings::TYPE_32SC1);
        cv::Mat image = input_bridge->image;
        image_ = image;
        Image2Labels(image,image_);
    }
    catch (cv_bridge::Exception& ex){
    ROS_ERROR("[SemanticCameraInterface] Failed to convert image");
    return;
    }

    cam_model_.fromCameraInfo(info_msg);
}

void SemanticCameraInterface::Initialize(std::string image_topic)
{
  sub_ = it_.subscribeCamera(image_topic, 1, &SemanticCameraInterface::ImageCb, this);
}

void SemanticCameraInterface::Image2Labels(cv::Mat &ip_image, cv::Mat &op_image){

    for(size_t i=0; i< ip_image.rows; i++){
        for(size_t j=0; j< ip_image.cols; j++){
            int intensity = ip_image.at<int32_t>(i, j);
            op_image.at<uchar>(i, j) =  RGB2Label(intensity);
        }
    }

}

uint8_t SemanticCameraInterface::RGB2Label(int ip){
    std::vector<int> class_c;

    class_c.push_back(1);
    class_c.push_back(4);
    class_c.push_back(2);
    class_c.push_back(3);
    class_c.push_back(0);

    size_t min_id=0;
    //ROS_INFO_STREAM("IP::"<<ip);
    for(size_t i=0; i<5; i++)
    {
        if(class_c[i] == ip){
            min_id = i;
        }
    }
    //ROS_INFO_STREAM("OP Class::"<<min_id);
    return min_id;
}

SemanticCameraInterface::SemanticCameraInterface(ros::NodeHandle &nh):CameraInterface(nh){}

//void LabelTensorInterface::LabelsTensorSub(const mavs_msgs::LabelsTensorConstPtr &label_msg){
    //ROS_INFO_STREAM("Recieved a label tensor message");
void LabelTensorInterface::LabelsTensorSub(const semantic_segmentation_msgs::LabelsTensorConstPtr &label_msg){
    if(!cam_model_.fromCameraInfo(label_msg->camera_info))
        ROS_ERROR_STREAM("Failed to load camera info of frame "<< label_msg->camera_info.header.frame_id);
    /*ROS_INFO_STREAM("Camera info message was processed");

    for(size_t i=0; i< label_msg->classes.size();i++){
        ROS_INFO_STREAM("Classes::"<<i<<"::"<<label_msg->classes[i]);
    }
    for(size_t i=0; i< label_msg->labels.layout.dim.size(); i++){
        ROS_INFO_STREAM("Reading label-tensor parameters::"<<"\n Dim desc::"<<label_msg->labels.layout.dim[i].label
                        <<"\n Size::"<<label_msg->labels.layout.dim[i].size<<
                        "\n Stride::"<<label_msg->labels.layout.dim[i].stride);
    }*/

    //ROS_INFO_STREAM("Data offset::"<<label_msg->labels.layout.data_offset);

    if(labels_data_.size() == 0){
        size_t total_size=1;
        uint32_t stride = 0;
        for(size_t i=0; i<label_msg->labels.layout.dim.size();i++){
            //ROS_INFO_STREAM("Dim size::"<<label_msg->labels.layout.dim[i].size <<"Stride::"<<label_msg->labels.layout.dim[i].stride);
            total_size = total_size*label_msg->labels.layout.dim[i].size;
            stride = label_msg->labels.layout.dim[i].stride;
        }
        labels_data_.resize(total_size*stride);
        //ROS_INFO_STREAM("Total size::"<<total_size);
    }
    //ROS_INFO_STREAM("Message size::"<<label_msg->labels.data.size()<<":: Local Size::"<<labels_data_.size());
    std::copy(std::begin(label_msg->labels.data), std::end(label_msg->labels.data), std::begin(labels_data_)); // All the data is in labels_data_
    labels_data_layout_ = label_msg->labels.layout;
    //ROS_INFO_STREAM("\n\n\nGot the label tensor, let's see its details");
    //ROS_INFO_STREAM("Camera Frame::"<<cam_model_.tfFrame());
    //ROS_INFO_STREAM("X Size::"<<label_msg->labels.layout.dim[1].size<<"Y Size::"<<label_msg->labels.layout.dim[1].size<<"Z Size::"<<label_msg->labels.layout.dim[0].size);

    //ROS_INFO_STREAM("Classes::");
    //for(size_t i=0; i<label_msg->classes.size();i++){
    //    ROS_INFO_STREAM(label_msg->classes[i]);
    //}
}

Eigen::Vector3d LabelTensorInterface::getRayAt(double x, double y){
    cv::Point2d px_cv(x,y);
    cv::Point3d pt_cv = cam_model_.projectPixelTo3dRay(px_cv);
    Eigen::Vector3d v = ca::point_cast<Eigen::Vector3d>(pt_cv);
    v.normalize();
    return v;
}

size_t LabelTensorInterface::getNumLabels(){
    if(labels_data_.size()==0)
        return 0;
    return labels_data_layout_.dim[0].size;
}


uint8_t LabelTensorInterface::getClassProbability(size_t row, size_t col, size_t c, bool &valid){

    size_t index = labels_data_layout_.dim[1].stride*row + labels_data_layout_.dim[2].stride*col;
    //ROS_INFO_STREAM("Valid::"<<(int)labels_data_[(index)]);
    if (labels_data_[(index)] != 255){
        ROS_ERROR_STREAM("Found Invalid Pixel::"<<labels_data_[(index)]);
        valid = false;
        return 0;
    }
    valid = true;
    return labels_data_[(index + c*labels_data_layout_.dim[0].stride)];
}

