#include "semantic_grid/utils.h"

Eigen::Vector3d CameraCoordinate2Ray(unsigned int x, unsigned int y, cv::Matx33f camera_intrinsics){
    cv::Matx31f hom_pt(x, y, 1);
    hom_pt = camera_intrinsics.inv()*hom_pt; //put in camera coordinates

    cv::Point3f origin(0,0,0);
    cv::Point3f direction(hom_pt(0),hom_pt(1),hom_pt(2));

    //To get a unit vector, direction just needs to be normalized
    direction *= 1/cv::norm(direction);
    Eigen::Vector3d v = ca::point_cast<Eigen::Vector3d>(direction);
    return v;
}

//void transformVector3d(const tf::Transform &transform, Eigen::Vector3d &vec)

// figure out image_geometry
// figure out geom_cast -- done
// figure out tf_utils -- done
// figure out camer_info to cv intrinsics --
// figure out gridmapping
