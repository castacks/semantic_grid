#include "semantic_grid/semantic_map_datatypes.h"

bool ca::SemanticGridCell::UpdateClassProperties(SemanticClass c, double height, double distance, double p){
    bool to_be_processed = false;

    double height_above_ground = height_mean_ - height;
    class_properties_[(size_t)c].upper_height_bound_
                                = std::max(height_above_ground, class_properties_[(size_t)c].upper_height_bound_);
    class_properties_[(size_t)c].lower_height_bound_
                                = std::min(height_above_ground, class_properties_[(size_t)c].lower_height_bound_);
    class_properties_[(size_t)c].height_initialized_ = true;
    min_observation_distance_ = std::min(distance,min_observation_distance_);
    if(height_above_ground < class_properties_[(size_t)c].possible_heights_[0]){
        class_properties_[(size_t)c].p_obs_for_ = class_properties_[(size_t)c].p_obs_for_ + p;
        class_properties_[(size_t)c].num_obs_for_ += 1;
        to_be_processed = true;
    }
    for(size_t i=0; i<p_classes_.size(); i++){
        if( i!= (size_t)c &&
            height_above_ground < 0.77*class_properties_[i].possible_heights_[0] &&
            height_above_ground > 0.1){

            class_properties_[(size_t)i].p_obs_against_ = class_properties_[(size_t)c].p_obs_against_ + p;
            class_properties_[(size_t)i].num_obs_against_ +=1;
            to_be_processed = true;
        }
    }
    return to_be_processed;
}


bool ca::SemanticGridCell::ObservationModel(SemanticClass a_c, double *p_z_given_a, double *p_z_given_nota, double distance, SemanticClass z_c, double p){
    *p_z_given_a = P_Z_Given_A(distance,z_c,a_c);
    *p_z_given_nota = P_Z_Given_NOTA(distance,z_c,a_c);
    return true;
}


void ca::SemanticGridCell::UpdateClassProbabilities(SemanticClass c, double p, double distance){
    for(size_t i=0; i<p_classes_.size(); i++){
        double p_z_given_a=0;
        double p_z_given_nota=0;
        if(ObservationModel((SemanticClass)i,&p_z_given_a, &p_z_given_nota, distance, c, p)){
            p_classes_[i] = probability_tools::UpdateLogOddsUsingProbability(p_classes_[i],p_z_given_a,p_z_given_nota);
        }
        else{
            ROS_ERROR_STREAM("This observation was not even modelled:: Class::"<<(int)c<<":: Distance::"<<distance<<":: P::"<<p);
        }

    }
}

//TODO: work on the multiple heights, work on the distance
bool ca::SemanticGridCell::NumRaysBasedObservationModel(){
        for(size_t i=0; i< p_classes_.size(); i++){
            p_classes_[i] = 0.5;
            if(class_properties_[i].num_obs_for_>class_properties_[i].num_obs_against_)
                p_classes_[i] = 0.7;
            else if(class_properties_[i].num_obs_for_<class_properties_[i].num_obs_against_)
                p_classes_[i] = 0.4;
        }
}
bool ca::SemanticGridCell::HeightBasedObservationModel(){
    // There are 4 cases here

    for(size_t i=0; i< p_classes_.size(); i++){
        SemanticClass c = (SemanticClass)i;
        uint8_t h_case = DetectCaseForHeightObservationModel(c);
        double expected_height = class_properties_[(int)c].possible_heights_[0];
        double actual_height = std::min(class_properties_[(int)c].ReportHeight(),class_properties_[(int)c].upper_height_bound_)
                            - class_properties_[(int)c].lower_height_bound_;

        double err = std::abs(expected_height - actual_height);
        if(h_case == 1){// Nothing is above or below
            // If it touches the ground, it has to be that class, if the height is within range, and the distance is acceptable -- as long as there is no sky
            // Otherwise it might be that class if we the height is within range and the distance is less            
            if(class_properties_[(int)c].lower_height_bound_<0.1 &&
                    (c == ca::SemanticClass::low_vegetation||c == ca::SemanticClass::ground_dirt||
                     c == ca::SemanticClass::ground_grass || c == ca::SemanticClass::car)){
                double prob0,prob1,prob2;
                prob0 = exp(-10.0*class_properties_[(int)c].lower_height_bound_);
                prob1 = exp(-1.0*err/class_properties_[(int)c].possible_heights_[0]);
                prob2 = (class_properties_[(int)c].p_obs_for_/class_properties_[(int)c].num_obs_for_);
                /*if(c == ca::SemanticClass::car)
                    ROS_ERROR_STREAM("Class::"<<i <<" Case::"<<(int)h_case<<"Prob::"<<prob0<<"::"<<prob1
                                     <<"::"<<prob2<<"\n Num For Against::"<< class_properties_[(int)c].num_obs_for_<<"::"
                                     <<class_properties_[(int)c].num_obs_against_
                                     <<"\nHeight Bounds::"<<class_properties_[(int)c].lower_height_bound_
                                     <<"::"<<class_properties_[(int)c].upper_height_bound_);*/
                p_classes_[(int)c] = prob0*prob1*prob2;
                //p_classes_[(int)c] = std::max(0.5,prob*P_Z_Given_A(2.0*min_observation_distance_,c,c));
            }
            else
                p_classes_[(int)c] =0.5;
        }
        else if(h_case == 2){ //Something is below but nothing above
            //upper height should be within range
            //lower height cannot be too far from class below or a certain threshold
            if(class_properties_[(int)c].lower_height_bound_<0.1 &&
                    (c == ca::SemanticClass::low_vegetation||c == ca::SemanticClass::ground_dirt||
                     c == ca::SemanticClass::ground_grass || c == ca::SemanticClass::car)){
                double prob0,prob1,prob2;
                prob0 = exp(-10.0*class_properties_[(int)c].lower_height_bound_);
                prob1 = exp(-1.0*err/class_properties_[(int)c].possible_heights_[0]);
                prob2 = (class_properties_[(int)c].p_obs_for_/class_properties_[(int)c].num_obs_for_);
                /*if(c == ca::SemanticClass::car)
                    ROS_ERROR_STREAM("Class::"<<i <<" Case::"<<(int)h_case<<"Prob::"<<prob0<<"::"<<prob1
                                     <<"::"<<prob2<<"\n Num For Against::"<< class_properties_[(int)c].num_obs_for_<<"::"
                                     <<class_properties_[(int)c].num_obs_against_
                                     <<"\nHeight Bounds::"<<class_properties_[(int)c].lower_height_bound_
                                     <<"::"<<class_properties_[(int)c].upper_height_bound_);*/
                p_classes_[(int)c] = prob0*prob1*prob2;
                //p_classes_[(int)c] = std::max(0.5,prob*P_Z_Given_A(2.0*min_observation_distance_,c,c));
            }
            else
                p_classes_[(int)c] =0.5;
        }
        else if(h_case == 3){//Something is up but nothing below
            // if height is less than expected-- not that class
            // lower height should be within range
            if(class_properties_[(int)c].lower_height_bound_<0.1 &&
                    (c == ca::SemanticClass::low_vegetation||c == ca::SemanticClass::ground_dirt||
                     c == ca::SemanticClass::ground_grass || c == ca::SemanticClass::car)){
                double prob0,prob1,prob2;
                prob0 = exp(-10.0*class_properties_[(int)c].lower_height_bound_);
                prob1 = exp(-1.0*err/class_properties_[(int)c].possible_heights_[0]);
                prob2 = (class_properties_[(int)c].p_obs_for_/class_properties_[(int)c].num_obs_for_);
                /*if(c == ca::SemanticClass::car)
                    ROS_ERROR_STREAM("Class::"<<i <<" Case::"<<(int)h_case<<"Prob::"<<prob0<<"::"<<prob1
                                     <<"::"<<prob2<<"\n Num For Against::"<< class_properties_[(int)c].num_obs_for_<<"::"
                                     <<class_properties_[(int)c].num_obs_against_
                                     <<"\nHeight Bounds::"<<class_properties_[(int)c].lower_height_bound_
                                     <<"::"<<class_properties_[(int)c].upper_height_bound_);*/

                p_classes_[(int)c] = prob0*prob1*prob2;
                //p_classes_[(int)c] = std::max(0.5,prob*P_Z_Given_A(2.0*min_observation_distance_,c,c));
            }
            else
                p_classes_[(int)c] =0.5;

        }
        else if(h_case == 4){//Something is up and below
            //lower height cannot be too far from class below or a certain threshold
            // if height is less than expected-- not that class
            if(class_properties_[(int)c].lower_height_bound_<0.1 &&
                    (c == ca::SemanticClass::low_vegetation||c == ca::SemanticClass::ground_dirt||
                     c == ca::SemanticClass::ground_grass || c == ca::SemanticClass::car)){
                double prob0,prob1,prob2;
                prob0 = exp(-10.0*class_properties_[(int)c].lower_height_bound_);
                prob1 = exp(-1.0*err/class_properties_[(int)c].possible_heights_[0]);
                prob2 = (class_properties_[(int)c].p_obs_for_/class_properties_[(int)c].num_obs_for_);
                /*if(c == ca::SemanticClass::car)
                    ROS_ERROR_STREAM("Class::"<<i <<" Case::"<<(int)h_case<<"Prob::"<<prob0<<"::"<<prob1
                                     <<"::"<<prob2<<"\n Num For Against::"<< class_properties_[(int)c].num_obs_for_<<"::"
                                     <<class_properties_[(int)c].num_obs_against_
                                     <<"\nHeight Bounds::"<<class_properties_[(int)c].lower_height_bound_
                                     <<"::"<<class_properties_[(int)c].upper_height_bound_);*/
                p_classes_[(int)c] = prob0*prob1*prob2;
                //p_classes_[(int)c] = std::max(0.5,prob*P_Z_Given_A(2.0*min_observation_distance_,c,c));
            }
            else
                p_classes_[(int)c] =0.5;
        }
        else if(h_case == 5){//Num against is more than num for
            /*if(c == ca::SemanticClass::vegetation)
                ROS_ERROR_STREAM("Class::"<<i <<" Case::"<<(int)h_case
                                 <<"\n Num For Against::"<< class_properties_[(int)c].num_obs_for_<<"::"
                                 <<class_properties_[(int)c].num_obs_against_
                                 <<"\nHeight Bounds::"<<class_properties_[(int)c].lower_height_bound_
                                 <<"::"<<class_properties_[(int)c].upper_height_bound_);*/
                p_classes_[(size_t)c] = 0.001;
        }
        else{
            // This is when the height is not initialized
            p_classes_[(int)c] =0.5;
        }
    }
}

uint8_t ca::SemanticGridCell::DetectCaseForHeightObservationModel(ca::SemanticClass c){
    if(class_properties_[(int)c].num_obs_for_ ==0 && class_properties_[(int)c].num_obs_against_ < 2)
        return 0;

    if(1.2*(double)class_properties_[(int)c].num_obs_for_ < (double)class_properties_[(int)c].num_obs_against_)
        return 5;


    if(!class_properties_[(int)c].height_initialized_){
        return 0;
    }
    double low_height = class_properties_[(int)c].upper_height_bound_;
    double high_height = class_properties_[(int)c].lower_height_bound_;

    for(size_t i=0; i<class_properties_.size(); i++){
        low_height=std::min(low_height,class_properties_[i].lower_height_bound_);
        high_height=std::max(high_height,class_properties_[i].upper_height_bound_);
    }

    if(low_height==class_properties_[(int)c].lower_height_bound_
            && high_height==class_properties_[(int)c].upper_height_bound_ )
        return 1;
    else if(low_height < class_properties_[(int)c].lower_height_bound_
            && high_height==class_properties_[(int)c].upper_height_bound_ )
        return 2;
    else if(low_height == class_properties_[(int)c].lower_height_bound_
            && high_height>class_properties_[(int)c].upper_height_bound_ )
        return 3;
    else if(low_height < class_properties_[(int)c].lower_height_bound_
            && high_height>class_properties_[(int)c].upper_height_bound_ )
        return 4;
}

double ca::SemanticGridCell::P_Z_Given_A(double distance, SemanticClass z_c, SemanticClass a_c){
    double y =0;
    if(z_c == a_c)
        y = 1/(1+std::exp(0.05*distance)) + 0.5;
    else
        y = 1-(1/(1+std::exp(0.05*distance)) + 0.5);

    return y;
}
double ca::SemanticGridCell::P_Z_Given_NOTA(double distance, SemanticClass z_c, SemanticClass a_c){
    double y=0;
    if(z_c == a_c)
        y = 1 -(1/(1+std::exp(0.05*distance)) + 0.5);
    else
        y = 1/(1+std::exp(0.05*distance)) + 0.5;

    return y;
}

void ca::SemanticGridCell::UpdateIntegratedLogOdds(){
    int increment_scale = 1;
    int increment_max = 10;

    int decrement_scale = 1;
    int decrement_min = -20;
    for(size_t i=0; i<p_classes_.size(); i++){
        if(p_classes_[i] != 0.5){
            if(p_classes_[i] > 0.5){
                int update = std::round(increment_scale*
                (class_properties_[i].num_obs_for_-
                class_properties_[i].num_obs_against_));
                update = std::min(update,increment_max);
                integrated_logodds_[i] += update;
            }
            else{
                int update = std::round(decrement_scale*
                (class_properties_[i].num_obs_for_-
                class_properties_[i].num_obs_against_));
                update = std::max(update,decrement_min);
                integrated_logodds_[i] += update;
            }
        }
    }
}


enum class SemanticClass{
    building = 0,
    car,
    ground_dirt,
    ground_grass,
    high_vegetation,
    low_vegetation
};


ca::SemanticGridCell::SemanticGridCell(){
    p_classes_.clear();
    ClassProperties c;
    for(size_t i=0; i<6; i++){
        p_classes_.push_back(0.0);
        class_properties_.push_back(c);
        integrated_logodds_.push_back(0);
    }

    integrated_logodds_[(int)SemanticClass::building] = -10;
    integrated_logodds_[(int)SemanticClass::car] = -10;
    integrated_logodds_[(int)SemanticClass::ground_dirt] = 10;
    integrated_logodds_[(int)SemanticClass::ground_grass] = -10;
    integrated_logodds_[(int)SemanticClass::high_vegetation] = -10;
    integrated_logodds_[(int)SemanticClass::low_vegetation] = -10;


    p_classes_[(int)SemanticClass::building] = 0.0;
    p_classes_[(int)SemanticClass::car] = 0.0;
    p_classes_[(int)SemanticClass::ground_dirt] = 0.6;
    p_classes_[(int)SemanticClass::ground_grass] = 0.0;
    p_classes_[(int)SemanticClass::high_vegetation] = 0.0;
    p_classes_[(int)SemanticClass::low_vegetation] = 0.0;

   class_properties_[(int)SemanticClass::building].possible_heights_.push_back(5);
   class_properties_[(int)SemanticClass::car].possible_heights_.push_back(1.2);
   class_properties_[(int)SemanticClass::ground_dirt].possible_heights_.push_back(0.2);
   class_properties_[(int)SemanticClass::ground_grass].possible_heights_.push_back(0.2);
   class_properties_[(int)SemanticClass::high_vegetation].possible_heights_.push_back(2.0);
   class_properties_[(int)SemanticClass::low_vegetation].possible_heights_.push_back(0.2);

    min_observation_distance_ = std::pow(10,6);
    casted_ = false;
}
