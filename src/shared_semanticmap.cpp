
#include "semantic_grid/shared_semanticmap.hpp"

void ca::SharedSemanticMap::AddToBeProcessed(size_t mem){

    std::vector<size_t>::iterator it;
     it = find (mem_to_process_.begin(), mem_to_process_.end(), mem);
     if (it == mem_to_process_.end())
        mem_to_process_.push_back(mem);
}


void ca::SharedSemanticMap::ProcessToBeProcessed(){
    for(size_t i=0; i<mem_to_process_.size();i++){        
        ca:SemanticGridCell& old_value = grid_data_[mem_to_process_[i]];
        old_value.HeightBasedObservationModel();
        //old_value.NumRaysBasedObservationModel();
        old_value.UpdateIntegratedLogOdds();
    }
}

double ca::SharedSemanticMap::GetRayHeight(Eigen::Vector3d& sensor_pos, Eigen::Vector3d& dir, Eigen::Vector2d& xy, bool& vertical)
{
    double z = 0;
    if(std::abs(dir.z()) < std::numeric_limits<double>::epsilon()){
        vertical = false;
        return (z+sensor_pos.z());
    }
    if((std::abs(dir.x()) + std::abs(dir.y())) < 2*std::numeric_limits<double>::epsilon()){
        vertical  = true;
        return 0;
    }
    vertical = false;
    if(dir.x() > std::numeric_limits<double>::epsilon()){
        // Use the X axis
        z = (dir.z()/dir.x())*(xy.x()-sensor_pos.x()) + sensor_pos.z();
    }
    else{// Use the Y axis
        z = (dir.z()/dir.y())*(xy.y()-sensor_pos.y()) + sensor_pos.z();
    }
    return z;
}

bool ca::SharedSemanticMap::UpdateCell(Eigen::Vector3d &sensor_pos, Eigen::Vector3d &dir, ca::SemanticClass& c ,int x, int y, bool endpt){
  //      ca::Profiler::tictoc("rgbd_grid_sensor::upprob");
    //ROS_INFO_STREAM("Called update cell::"<<x<<"::"<<y);
    bool cont_raycasting = true;
    if(!grid2_->is_inside_grid(x,y))return false;
    //ROS_INFO("It is inside grid");
    mem_ix_t mem_ix = grid2_->grid_to_mem(x, y);
    ca:SemanticGridCell old_value = grid_data_[mem_ix];
    ca::FixedGrid2f::Vec2 v = grid2_->grid_to_world(x,y);
    Eigen::Vector2d vv; vv.x() = v.x(); vv.y() = v.y();
    bool vertical = false;
    double height = GetRayHeight(sensor_pos, dir,vv,vertical);
    if((old_value.height_mean_ <= height) || vertical){
        cont_raycasting = false;        
        //ROS_INFO_STREAM("Updating Class::"<<(int)c);
        //old_value.UpdateClassProbabilities(c);
        grid_data_[mem_ix] = old_value;
    }
    return cont_raycasting;
  //      ca::Profiler::tictoc("rgbd_grid_sensor::upprob");
}


bool ca::SharedSemanticMap::UpdateCell3D(ca::SemanticClass& c, double p ,int x, int y, int z, int s_x,int s_y, int s_z){
  //      ca::Profiler::tictoc("rgbd_grid_sensor::upprob");
    //ROS_INFO_STREAM("In grid::"<<x<<"::"<<y<<"::"<<z);
    bool cont_raycasting = true;
    if(!grid2_->is_inside_grid(x,y))    return false;
    //ROS_INFO("It is inside grid");
    mem_ix_t mem_ix = grid2_->grid_to_mem(x, y);
    ca:SemanticGridCell& old_value = grid_data_[mem_ix];

    double height = z;
    float distance = std::sqrt(std::pow(s_x-x,2)*grid2_->resolution() + std::pow(s_y-y,2)*grid2_->resolution() + std::pow(s_z-z,2));
    if((old_value.height_mean_ <= height)){
        height = old_value.height_mean_;
        cont_raycasting = false;
        grid_data_[mem_ix].casted_ = true;
    }
    if(distance > min_update_distance_){
        if(old_value.UpdateClassProperties(c,height,distance,p))
            AddToBeProcessed(mem_ix);
    }
    //old_value.HeightBasedObservationModel();

    return cont_raycasting;
  //      ca::Profiler::tictoc("rgbd_grid_sensor::upprob");
}



void ca::SharedSemanticMap::ProcessRay(Eigen::Vector3d& dir, Eigen::Vector3d& pos, double range, ca::SemanticClass& c, double prob)
{
    //proxy_->WaitForWriteLock();
    //ignoring invalid depth points for now.. we are not producing them in
    //depth_to_cloud anyways
    //TODO can we change the update values which are set in occ_raycasting.hpp
    //here? What if we want to vary them between projects

    //ca::Profiler::tictoc("RayCasting");
    //ROS_DEBUG("RGBDGridSensor:raycasting:got write lock");

    FixedGrid2f::Vec2 p(pos.x(),pos.y());
    //ROS_INFO_STREAM("POS::"<<pos.x()<<"::"<<pos.y());
    //Vec2Ix sensor_pos_ijj = grid2_->world_to_grid(p);
    //ROS_INFO_STREAM("Its outside box::"<<sensor_pos_ijj.x()<<"::"<<sensor_pos_ijj.y());
    if(grid2_->is_inside_box(p)){        
        Vec2Ix sensor_pos_ij = grid2_->world_to_grid(p);
        //ROS_INFO_STREAM("Its inside box::"<<sensor_pos_ij.x()<<"::"<<sensor_pos_ij.y());
        dir = pos+range*dir;
        FixedGrid2f::Vec2 r(dir.x(),dir.y());
        Vec2Ix ray_end_pos_ij =  grid2_->world_to_grid(r);
        Vec3Ix sensor_pos_ijk;
        sensor_pos_ijk.x() = sensor_pos_ij.x(); sensor_pos_ijk.y() = sensor_pos_ij.y();
        sensor_pos_ijk.z() = round(pos.z());

        Vec3Ix ray_end_ijk;
        ray_end_ijk.x() = ray_end_pos_ij.x(); ray_end_ijk.y() = ray_end_pos_ij.y();
        ray_end_ijk.z() = round(dir.z());

        boost::function<bool (int,int,int)> UpCell( boost::bind( &SharedSemanticMap::UpdateCell3D, this, c, prob,_1,_2,_3,
                                                                 sensor_pos_ijk.x(), sensor_pos_ijk.y(),sensor_pos_ijk.z() ) );
        ca::bresenham_trace(sensor_pos_ijk,ray_end_ijk,UpCell);
    }
    //ca::Profiler::tictoc("RayCasting");
    //ROS_DEBUG("RGBDGridSensor:raycasting:time to free write lock");
    //proxy_->FreeWriteLock();
    //ROS_DEBUG("RGBDGridSensor:raycasting:write lock freed");
}
